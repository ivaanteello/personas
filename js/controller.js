miApp.controller('appController',['$scope', '$http', function($scope, $http){

    $scope.mainUrl = "http://databaseremote.esy.es/personaCoche/bl/";
    $scope.isEdit = false;
    $scope.loadingVisible = true;
    $scope.popupVisible = false;
    $scope.infoVisible = true;
    $scope.popupPeopleVisible = false;
    $scope.inProgress = true;
    $scope.buttonsVisible = false;
    $scope.popupCarVisible = false;
    $scope.messageVisible = true;
    $scope.removePopupVisible = false;

    /*Consultar todas las personas*/
    $http.post($scope.mainUrl + "PersonaBL.php", {metodo: "getAllPersona"})
        .success(function(data){
        console.log(data);
        $scope.loadingVisible = false;
        switch(data.success){
            case 0:
                $scope.people = data.personas;
                break;
            default:
                $scope.message = data.message;
                $scope.popupVisible = true;
                break;
        }
    })
        .error(function(data){
        console.log(data);
        $scope.loadingVisible = false;
    });

    $scope.createPeople = function(){
        return{
            Id: null,
            Nombre: null,
            ApellidoPaterno: null,
            ApellidoMaterno: null,
            Fecha: null
        }
    };
    $scope.thePeople = $scope.createPeople();

    /*Muestra el popup para agregar persona*/
    $scope.showPopupPeople = function(title, action){
        $scope.infoVisible = true;
        $scope.popupPeopleVisible = true;
        $scope.titlePopup = title;
        $scope.actionButton = action;
    };
    /*Oculta el popup para agregar persona*/
    $scope.hidePopupPeople = function(){
        $scope.isEdit = false;
        $scope.clearFormPeople();
        $scope.birthdate = null;
        $scope.popupPeopleVisible = false;
    };
    /*Borra el formulario*/
    $scope.clearFormPeople = function(){
        $scope.thePeople = $scope.createPeople();
    };

    /*Método para guardar y editar la persona*/
    $scope.savePeople = function(){
        if($scope.inProgress){
            $scope.inProgress = false;
            $scope.loadingVisible = true;
            if(!$scope.isEdit){
                $scope.id = null;
            }
            console.log($scope.thePeople);
            $http.post($scope.mainUrl + "PersonaBL.php",{metodo: 'savePersona', id: $scope.thePeople.Id, nombre: $scope.thePeople.Nombre, apPaterno: $scope.thePeople.ApellidoPaterno, apMaterno: $scope.thePeople.ApellidoMaterno, fecha: $scope.birthdate.valueOf()})
                .success(function(data){
                console.log(data);
                $scope.loadingVisible = false;
                switch(data.success){
                    case 0:
                        $scope.people = data.personas;
                        $scope.hidePopupPeople();
                        break;
                    default:
                        $scope.message = data.message;
                        $scope.infoVisible = false;
                        break;
                }
                $scope.inProgress = true;
                $scope.isEdit = false;
                $scope.buttonsVisible = false;
            })
                .error(function(data){
                console.log(data);
                $scope.inProgress = true;
                $scope.loadingVisible = false;
            });
        }
    };

    /*Muestra los botones editar persona y agregar coche*/
    $scope.showButtons = function(people){
        $scope.buttonsVisible = true;
        $scope.thePeople = people;
    };

    /*Método para preparar la edición de una persona*/
    $scope.editPeople = function(){
        $scope.isEdit = true;
        $scope.showPopupPeople('Editar Persona', 'Actualizar');
        var fechaLong = parseInt($scope.thePeople.Fecha);
        $scope.birthdate = new Date(fechaLong);
    };

    $scope.createCar = function(){
        return{
            Id: null,
            Marca: null,
            Modelo: null,
            Anyo: null,
            Color: null,
            PersonaId: null
        }
    };
    $scope.theCar = $scope.createCar();

    /*las opciones del combobox se llenan de un json*/
    $http.get('json/color.json').success(function(data){
        $scope.color = data;
    });

    /*Muestra el popup para agregar un coche*/
    $scope.showPopupCar = function(title, action){
        $scope.infoVisible = true;
        $scope.popupCarVisible = true;
        $scope.titlePopup = title;
        $scope.actionButton = action;
    };

    /*Ocultar el popup para agregar un coche*/
    $scope.hidePopupCar = function(){
        $scope.isEdit = false;
        $scope.clearFormCar();
        $scope.popupCarVisible = false;
    };

    /*Borra el formulario*/
    $scope.clearFormCar = function(){
        $scope.theCar = $scope.createCar();
        $scope.year = "";
        $scope.paint = "";
    };

    /*Método para guardar y editar el coche*/
    $scope.saveCar = function(){
        if($scope.inProgress){
            $scope.inProgress = false;
            $scope.loadingVisible = true;
            var idPeople;
            if(!$scope.isEdit){
                idPeople = $scope.thePeople.Id;
            }
            else{
                idPeople = $scope.theCar.PersonaId;
            }
            console.log($scope.theCar);
            $http.post($scope.mainUrl + "CocheBL.php",{metodo: 'saveCoche', id: $scope.theCar.Id, marca: $scope.theCar.Marca, modelo: $scope.theCar.Modelo, anyo: $scope.year.toString(), color: $scope.paint.nombre, personaId: idPeople})
                .success(function(data){
                console.log(data);
                $scope.loadingVisible = false;
                switch(data.success){
                    case 0:
                        if(!$scope.isEdit){
                            $scope.people = data.getAllPersona.personas;
                        }
                        else{
                            $scope.cars = data.getCochesByPersonaId.coches;
                        }
                        $scope.hidePopupCar();
                        break;
                    default:
                        $scope.message = data.message;
                        $scope.infoVisible = false;
                        break;
                }
                $scope.inProgress = true;
                $scope.isEdit = false;
            })
                .error(function(data){
                console.log(data);
                $scope.inProgress = true;
                $scope.loadingVisible = false;
            });
        }
    };

    /*Método para ver coches por persona*/
    $scope.showCars = function(people){
        if($scope.inProgress){
            $scope.inProgress = false;
            $scope.loadingVisible = true;
            $http.post($scope.mainUrl + "CocheBL.php",{metodo: 'getCochesByPersonaId', personaId: people.Id})
                .success(function(data){
                console.log(data);
                $scope.loadingVisible = false;
                switch(data.success){
                    case 0:
                        $scope.cars = data.coches;
                        $scope.messageVisible = true;
                        break;
                    case 1:
                        $scope.cars = [];
                        $scope.messageVisible = false;
                        break;
                    default:
                        $scope.message = data.message;
                        $scope.popupVisible = true;
                        break;
                }
                $scope.inProgress = true;
            })
                .error(function(data){
                console.log(data);
                $scope.inProgress = true;
                $scope.loadingVisible = false;
            });
        }
    };

    /*Método para preparar la edición de un coche*/
    $scope.editCar = function(car){
        $scope.theCar = car;
        $scope.year = parseInt(car.Anyo);
        for(var i=0; i<$scope.color.length; i++){
            if($scope.color[i].nombre == car.Color){
                $scope.paint = $scope.color[i];
            }
        }
        $scope.isEdit = true;
        $scope.showPopupCar('Editar Coche', 'Actualizar');
    };

    /*Muestra el popup para confirmar el borrado*/
    $scope.showPopupRemoveCar = function(car){
        $scope.theCar = car;
        $scope.brand = car.Marca + " " + car.Modelo;
        $scope.removePopupVisible = true;
    }

    /*Método para borrar coches*/
    $scope.deleteCar = function(){
        if($scope.inProgress){
            $scope.inProgress = false;
            $http.post($scope.mainUrl + "CocheBL.php",{metodo: 'deleteCocheById', id: $scope.theCar.Id, personaId: $scope.theCar.PersonaId})
                .success(function(data){
                console.log(data);
                $scope.loadingVisible = false;
                switch(data.success){
                    case 0:
                        $scope.people = data.getAllPersona.personas;
                        $scope.cars = data.getCochesByPersonaId.coches;
                        $scope.removePopupVisible = false;
                        if(data.getCochesByPersonaId.success == 1){
                            $scope.cars = [];
                            $scope.messageVisible = false;
                            $scope.removePopupVisible = false;
                        }
                        break;
                    default:
                        $scope.removePopupVisible = false;
                        $scope.message = data.message;
                        $scope.popupVisible = true;
                        break;
                }
                $scope.inProgress = true;
                $scope.isEdit = false;
            })
                .error(function(data){
                console.log(data);
                $scope.inProgress = true;
                $scope.loadingVisible = false;
            });
        }
    }
}]);
